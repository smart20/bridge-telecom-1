exports = module.exports = ['$scope', 'Map', '$location', function ($scope, Map, $location) {
  // $scope.googleMapsUrl="https://maps.googleapis.com/maps/api/js?key=AIzaSyDArQiA7qyfvin0AtB7Sbzr5gSXzW6OMww";
  
  angular.element(document).ready(function () {
    if ($location.$$path == "/contacts")
        DG.then(function() {
            var map = DG.map('map', {
            center: [62.024274, 129.726251],
            zoom: 17,
            dragging : true,
            touchZoom: false,
            scrollWheelZoom: false,
            doubleClickZoom: true,
            boxZoom: true,
            geoclicker: false,
            zoomControl: true,
            fullscreenControl: true
            });

            DG.marker([62.024274, 129.726251]).addTo(map).bindPopup('Октябрьская 1/1, офис 405');
        });
  });
  // production
  $scope.googleMapsUrl="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaNyZU96xl-YZ6U5YpFHZxiB0RKIWoQbE";
  $scope.markers2 = [
      {coordinates:[62.151872, 129.792258], properties: {balloonContent: 'Якутск'}},
      {coordinates:[63.746424, 121.634535], properties: {balloonContent: 'Вилюйск'}},
      {coordinates:[56.669770, 124.671613], properties: {balloonContent: 'Нерюнгри'}},
      {coordinates:[61.479805, 129.128757], properties: {balloonContent: 'Покровск'}},
      {coordinates:[66.417055, 112.321323], properties: {balloonContent: 'Удачный'}},
      {coordinates:[58.607679, 125.365840], properties: {balloonContent: 'Алдан'}},
      {coordinates:[60.726855, 114.931319], properties: {balloonContent: 'Ленск'}},
      {coordinates:[63.283851, 118.323919], properties: {balloonContent: 'Нюрба'}},
      {coordinates:[67.464981, 153.698560], properties: {balloonContent: 'Среднеколымск'}},
      {coordinates:[67.552181, 133.399554], properties: {balloonContent: 'Верхоянск'}},
      {coordinates:[62.531060, 113.992679], properties: {balloonContent: 'Мирный'}},
      {coordinates:[60.380348, 120.434264], properties: {balloonContent: 'Олекминск'}},
      {coordinates:[58.949225, 126.253407], properties: {balloonContent: 'Томмот'}}
  ];
  $scope.markers = [
      {coordinates:[68.541956, 146.202438], properties: {balloonContent: 'п. Белая Гора'}},
      {coordinates:[58.607679, 125.365840], properties: {balloonContent: 'г. Алдан'}},
      {coordinates:[62.361478, 133.563802], properties: {balloonContent: 'с. Ытык-Кюель'}},
      {coordinates:[70.623582, 147.906412], properties: {balloonContent: 'п. Чокурдах'}},
      {coordinates:[60.899335, 131.966162], properties: {balloonContent: 'с. Амга'}},
      {coordinates:[71.965661, 114.094238], properties: {balloonContent: 'с. Саскылах'}},
      {coordinates:[71.663182, 128.811602], properties: {balloonContent: 'п. Тикси'}},
      {coordinates:[63.447262, 120.309830], properties: {balloonContent: 'с. Верхневилюйск'}},
      {coordinates:[65.735833, 150.868962], properties: {balloonContent: 'п. Зырянка'}},
      {coordinates:[67.662397, 134.621861], properties: {balloonContent: 'п. Батагай'}},
      {coordinates:[63.746424, 121.634535], properties: {balloonContent: 'г. Вилюйск'}},
      {coordinates:[62.098359, 126.699456], properties: {balloonContent: 'с. Бердигестях'}},
      {coordinates:[66.781530, 123.367241], properties: {balloonContent: 'с. Жиганск'}},
      {coordinates:[63.936190, 127.482936], properties: {balloonContent: 'п. Сангар'}},
      {coordinates:[60.726855, 114.931319], properties: {balloonContent: 'г. Ленск'}},
      {coordinates:[61.737797, 130.284902], properties: {balloonContent: 'с. Майя'}},
      {coordinates:[62.531060, 113.992679], properties: {balloonContent: 'г. Мирный'}},
      {coordinates:[66.447610, 143.223575], properties: {balloonContent: 'с. Хону'}},
      {coordinates:[62.714943, 129.662666], properties: {balloonContent: 'с. Намцы'}},
      {coordinates:[68.768005, 161.358378], properties: {balloonContent: 'п. Черский'}},
      {coordinates:[63.283851, 118.323919], properties: {balloonContent: 'г. Нюрба'}},
      {coordinates:[64.562498, 143.214233], properties: {balloonContent: 'п. Усть-Нера'}},
      {coordinates:[60.380348, 120.434264], properties: {balloonContent: 'г. Олекминск'}},
      {coordinates:[68.501552, 112.453056], properties: {balloonContent: 'с. Оленек'}},
      {coordinates:[61.479805, 129.128757], properties: {balloonContent: 'г. Покровск'}},
      {coordinates:[67.464981, 153.698560], properties: {balloonContent: 'г. Среднеколымск'}},
      {coordinates:[62.151053, 117.647008], properties: {balloonContent: 'с. Сунтар'}},
      {coordinates:[62.644578, 135.580049], properties: {balloonContent: 'п. Хандыга'}},
      {coordinates:[62.673607, 131.163594], properties: {balloonContent: 'с. Борогонцы'}},
      {coordinates:[60.424138, 134.540734], properties: {balloonContent: 'п. Усть-Мая'}},
      {coordinates:[69.347593, 139.949288], properties: {balloonContent: 'п. Депутатский'}},
      {coordinates:[61.996893, 132.445458], properties: {balloonContent: 'с. Чурапча'}},
      {coordinates:[67.797046, 130.401773], properties: {balloonContent: 'с. Батагай-Алыта'}}
  ];
}];