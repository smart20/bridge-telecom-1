exports = module.exports = ['$scope', 'Home', function ($scope, Home) {
  
  Home.query().$promise.then(function(data){
    $scope.posts = data[0].posts;
  });

}];