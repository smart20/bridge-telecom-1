exports = module.exports = ['$scope', function ($scope) {
  $scope.myCustomOptions = {
    cellSelector: '.slider__slide',
    autoPlay: 3000,
    draggable: false,
    pauseAutoPlayOnHover: false,
    pageDots: false,
    wrapAround: true
  };
}];