module.exports = ['$resource', function ($resource) {
  return $resource('api/vacancies/:id.json');
}];